﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryController : MonoBehaviour {

	public GameObject storyCanvas;

	private void Awake (){
		Time.timeScale = 0f;
	}

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown) {
			Time.timeScale = 1f;
			storyCanvas.SetActive (false);
		}
	}
}
