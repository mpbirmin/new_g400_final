﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawnerScript : MonoBehaviour {

	public GameObject blockToSpawn;
	public float startWait;
	public float loopWait;
	public Transform blockSpawnPosition;


	// Use this for initialization
	void Start () {
			StartCoroutine (BlockDropper ());
		}

	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator BlockDropper(){
		yield return new WaitForSeconds (startWait);
		while (true) {
			Instantiate (blockToSpawn, blockSpawnPosition);
			yield return new WaitForSeconds (loopWait);
		}
	}
}
