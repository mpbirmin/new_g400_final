﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpinScript : MonoBehaviour {

	public float spin = 0f;
	public float spinSpeed = 1f;

	private bool touchFloor = false;
	public Rigidbody2D rb;
	public float currentAngle = 0f;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!touchFloor) {
			Spinning ();
		}
	}
	void Spinning(){
		if (Time.time - spin >= spinSpeed) {
			rb.MoveRotation(currentAngle + 90f);
			currentAngle = currentAngle + 90f;
			spin = Time.time;
		}
	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag("Ground") || other.CompareTag("Block")){
			print ("I should stop spinning now.");
			touchFloor = true;
		}
	}
}
