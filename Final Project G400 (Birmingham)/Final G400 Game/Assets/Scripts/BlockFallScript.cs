﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockFallScript : MonoBehaviour {

	public float fall = 0f;
	public float fallSpeed = 1f;
	public float delay = 3f;

	private bool touchFloor = false;
	public Rigidbody2D rb;
	public AudioSource fallNoise;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		fallNoise = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!touchFloor) {
			Falling ();
		}
	}
	void Falling () {

		if (Time.time - fall >= fallSpeed) {
			rb.MovePosition(transform.position + new Vector3 (0, -1f, 0));
			fall = Time.time;
		}
	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag("Ground") || other.CompareTag("Block")){
			touchFloor = true;
			fallNoise.Play();
		}
	}
}