﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockDestroy : MonoBehaviour {

	private bool touchFloor = false;
	public float delay = 3f;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		if (touchFloor)
			WaitAndDestroy ();
		}

	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag ("Ground") || other.CompareTag ("Block")) {
			touchFloor = true;
		}
		if (other.CompareTag ("Pit")){
			Destroy (gameObject);
		}
	}

	void WaitAndDestroy(){
		Destroy (gameObject, delay);
	}
}
